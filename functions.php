<?php

add_action( 'wp_enqueue_scripts', 'enqueue_parent_styles' );
function enqueue_parent_styles() {
	wp_enqueue_style( 'parent-style', get_template_directory_uri().'/style.css' );
}

add_action( 'wp_enqueue_scripts', 'protomaker_js_scripts' );
function protomaker_js_scripts() {
   // wp_enqueue_script( 'protomaker-slideshow-script', get_stylesheet_directory_uri() . '/js/bg-slideshow.js', array(), true, true );
   // wp_enqueue_script( 'protomaker-topbar-script', get_stylesheet_directory_uri() . '/js/sticky-topbar.js', array(), true, true );
   // wp_enqueue_script( 'protomaker-fullpage-script', get_stylesheet_directory_uri() . '/js/fullpage.js', array(), true, true );
   wp_enqueue_script( 'protomaker-custom-scripts', get_stylesheet_directory_uri() . '/js/custom.js', array(), true, true );
   // wp_enqueue_script( 'protomaker-dragndrop-script', get_stylesheet_directory_uri() . '/js/dragndrop-upload.js', array(), true, true );
}

add_action('init', 'protomaker_header_footer' );
function protomaker_header_footer(){
    remove_action( 'storefront_header', 'storefront_primary_navigation', 50 );
    remove_action( 'storefront_header', 'storefront_secondary_navigation', 30 );
    remove_action( 'storefront_footer', 'storefront_credit', 20 );
    add_action( 'storefront_header', 'one_page_menu', 50 );
    add_action('storefront_footer', 'storefront_secondary_navigation', 30);
    // add_action('storefront_footer', 'language_switcher', 55);
    add_action('storefront_header', 'language_switcher', 60);
    add_action('storefront_footer', 'powered_by_see', 10);
}

// Remove default storefront fonts
add_action( 'wp_enqueue_scripts', 'storefront_remove_google_fonts', 100);
function storefront_remove_google_fonts() {
    wp_dequeue_style( 'storefront-fonts' );
}

// Remove search from storefront header
add_action( 'init', 'jk_remove_storefront_header_search' );
function jk_remove_storefront_header_search() {
  remove_action( 'storefront_header', 'storefront_product_search', 40 ); 
}

// Allowing file types in File Uploads that are not normally allowed by WordPress
add_filter( 'ninja_forms_upload_mime_types_whitelist', 'my_ninja_forms_upload_mime_types_whitelist' );
function my_ninja_forms_upload_mime_types_whitelist( $types ) {
//$types['esx'] = 'application/octet-stream';
return $types;
}

add_filter( 'ninja_forms_upload_check_mime_types_whitelist', '__return_false' );

function one_page_menu() {
  $menuLocations = get_nav_menu_locations(); // Get our nav locations (set in our theme, usually functions.php)
                                             // This returns an array of menu locations ([LOCATION_NAME] = MENU_ID);

  $menuID = $menuLocations['primary']; // Get the *primary* menu ID
  $mobileMenuID = $menuLocations['handheld']; // Get the *primary* menu ID

  $primaryNav = wp_get_nav_menu_items($menuID); // Get the array of wp objects, the nav items for our queried location.
  $mobileNav = wp_get_nav_menu_items($mobileMenuID);

?>

<div class="nav-icon">
  <div></div>
</div>
<nav id="site-navigation" class="main-navigation" role="navigation">
  <div class="primary-navigation">
    <ul id="menu-main-menu" class="menu nav-menu">
<?php

foreach ( $primaryNav as $navItem ) {
  $pageTemplate = get_page_template_slug( $navItem->object_id );
  // echo print_r($navItem);
  if ($pageTemplate == 'one-page.php') {
    echo '<li><a href="#'.get_section_href($navItem->title).'" class="nav-link" title="'.$navItem->title.'">'.$navItem->title.'</a></li>';
  }
  else {
    echo '<li><a href="'.$navItem->url.'" class="nav-link" title="'.$navItem->title.'">'.$navItem->title.'</a></li>';
  }
}
?>
    </ul>
  </div>
  <div class="mobile-navigation">
    <ul id="menu-mobile-menu" class="mobile-menu">
<?php

foreach ( $mobileNav as $navItem ) {
  $pageTemplate = get_page_template_slug( $navItem->object_id );
  // echo print_r($navItem);
  if ($pageTemplate == 'one-page.php') {
    echo '<li><a href="#'.get_section_href($navItem->title).'" class="nav-link" title="'.$navItem->title.'">'.$navItem->title.'</a></li>';
  }
  else {
    echo '<li><a href="'.$navItem->url.'" class="nav-link" title="'.$navItem->title.'">'.$navItem->title.'</a></li>';
  }
}
?>
    </ul>
    <?php echo rarus_polylang_languages(); ?>
  </div>
</nav>

<?php
  return;
}

function powered_by_see() {
  ?>

  <div class="by-see">
    <a href="https://see.inc" class="text-secondary"><?php echo date("Y"); ?> © SEE</a>
    <a href="https://www.facebook.com/protomaker.eu" class="ic-fb"></a>
    <a href="https://www.instagram.com/protomaker.eu" class="ic-insta"></a>
  </div>

  <?php
  return;
}

function language_switcher() {
/*
pll_the_languages($args);

$args is an optional array parameter. Options are:

    ‘dropdown’ => displays a list if set to 0, a dropdown list if set to 1 (default: 0)
    ‘show_names’ => displays language names if set to 1 (default: 1)
    ‘display_names_as’ => either ‘name’ or ‘slug’ (default: ‘name’)
    ‘show_flags’ => displays flags if set to 1 (default: 0)
    ‘hide_if_empty’ => hides languages with no posts (or pages) if set to 1 (default: 1)
    ‘force_home’ => forces link to homepage if set to 1 (default: 0)
    ‘echo’ => echoes if set to 1, returns a string if set to 0 (default: 1)
    ‘hide_if_no_translation’ => hides the language if no translation exists if set to 1 (default: 0)
    ‘hide_current’=> hides the current language if set to 1 (default: 0)
    ‘post_id’ => if set, displays links to translations of the post (or page) defined by post_id (default: null)
    ‘raw’ => use this to create your own custom language switcher (default:0)

More: https://polylang.wordpress.com/documentation/documentation-for-developers/functions-reference/

  <ul id="language_switcher"><?php pll_the_languages(array('show_flags'=>1));?></ul>
  <ul id="language_switcher"><?php pll_the_languages(array('dropdown' => 1,'display_names_as' => 'slug'));?></ul>
*/

  echo rarus_polylang_languages();

  return;
}

/**
 * Show Polylang Languages with Custom Markup
 * @param  string $class Add custom class to the languages container
 * @return string        
 */
function rarus_polylang_languages( $class = '' ) {

  if ( ! function_exists( 'pll_the_languages' ) ) return;

  // Gets the pll_the_languages() raw code
  $languages = pll_the_languages( array(
    'display_names_as'       => 'slug',
    'hide_if_no_translation' => 1,
    'raw'                    => true
  ) ); 

  $output = '';

  // Checks if the $languages is not empty
  if ( ! empty( $languages ) ) {

    // Creates the $final_output variable with current language
    $final_output = '<div class="language_switcher"><button class="current">';

    // Creates the $output variable with languages container
    $output = '<div class="languages' . ( $class ? ' ' . $class : '' ) . '">';

    // Runs the loop through all languages
    foreach ( $languages as $language ) {

      // Variables containing language data
      $id             = $language['id'];
      $slug           = $language['slug'];
      $url            = $language['url'];
      $current        = $language['current_lang'] ? ' languages__item--current' : '';
      $no_translation = $language['no_translation'];

      // Checks if the page has translation in this language
      if ( ! $no_translation ) {
        // Check if it's current language
        if ( $current ) {
          // Output the language in a <span> tag so it's not clickable
          $output .= "<span class=\"languages__item$current\">$slug</span>";
          $final_output .= "$slug";
        } else {
          // Output the language in an anchor tag
          $output .= "<a href=\"$url\" class=\"languages__item$current\">$slug</a>";
        }
      }

    }

    $output .= '</div>';

  }

  return $final_output . '</button>' . $output . '</div>';
}

//Page Slug Body Class
function add_slug_body_class( $classes ) {
  global $post;

  if ( isset( $post ) ) {
    $classes[] = $post->post_type . '-' . $post->post_name;
  }

  return $classes;
}
add_filter( 'body_class', 'add_slug_body_class' );


/**
 * Outputs localized string if polylang exists or outputs not translated one as a fallback
 *
 * @param $string
 *
 * @return  void
 */
function pl_e( $string = '' ) {
    if ( function_exists( 'pll_e' ) ) {
        pll_e( $string );
    } else {
        echo $string;
    }
}

/**
 * Returns translated string if polylang exists or outputs not translated one as a fallback
 *
 * @param $string
 *
 * @return string
 */
function pl__( $string = '' ) {
    if ( function_exists( 'pll__' ) ) {
        return pll__( $string );
    }

    return $string;
}

// these function prefixes can be either you are comfortable with.

function your_prefix_after_setup_theme() {

   // register our translatable strings - again first check if function exists.

    if ( function_exists( 'pll_register_string' ) ) {

        pll_register_string( 'HomeHeader', '3D&nbsp;print estimate', 'Protomaker', false );
        pll_register_string( 'HomeButtonTitle', 'Upload model', 'Protomaker', false );
        pll_register_string('HomeButtonSubtitle', 'Or simply drag & drop the file here', 'Protomaker', false);
        pll_register_string('HomeSSL', 'SSL secured. Your data is safe.', 'Protomaker', false);

    }
}
add_action( 'after_setup_theme', 'your_prefix_after_setup_theme' );

function get_section_href($string) {
  $href = preg_replace('/[^A-Za-z0-9\-]/', '', $string);;
  return strtolower($href);
}

// Determine request form language
function getFormInCurrentLanguage() {
  $lang = substr(get_locale(), 0, 2);
  $forms = Ninja_Forms()->form()->get_forms();
  $ninjaForm = 0;
  foreach ( $forms as $form ) {
    $form_id  = $form->get_id();
    $form_name  = $form->get_setting( 'title' );
    $title_lang = strtolower(substr($form_name, -2, 2));

    if ($lang == $title_lang) {
      // Returns an array of Field Models
      Ninja_Forms()->display( $form_id );
    }
  }
}

?>

