<?php
/*
 *	This page is used to display the static frontpage. 
*/
 
// Fetch theme header template
get_header();

// Get only one-page pages
$template = 'one-page.php';
$pages = get_pages( array( 'sort_column' => 'menu_order',
	'sort_order' => 'desc',
	'meta_key' => '_wp_page_template',
    'meta_value' => $template ) );
$technology_sections = array();
$sections = array();
$technology_title = '';

foreach ($pages as $page_data) {
     $content = apply_filters('the_content', $page_data->post_content);
     // echo print_r($page_data);
     
     if ($page_data->post_id == '75' || $page_data->post_id == '78' || $page_data->post_id == '260' || $page_data->post_id == '237') {
     	// Technologies parent page should not be displayed
     	$technology_title = get_section_href($page_data->post_title);
     }
     elseif ($page_data->post_parent == '75' || $page_data->post_parent == '78' || $page_data->post_parent == '260' || $page_data->post_parent == '237') {
     	array_push($technology_sections,$page_data);
     }
     else {
     	// echo print_r($page_data);
     	array_push($sections,$page_data);
     }
}

?>

<ul id="home" class="slides">
    <li class="slide showing"></li>
    <li class="slide"></li>
    <li class="slide"></li>
</ul>

<div class="home-container">
	<h1 class="title"><?php pll_e('3D&nbsp;print estimate'); ?></h1>
	<?php //echo print_r($ninjaFields);
	getFormInCurrentLanguage();
	//echo print_r(error_get_last()); ?> 
	<!-- <button class="button button-upload"><span><?php pll_e('Upload model'); ?></span></button> -->
	<!-- <span class="button-subtitle text-secondary"><?php pll_e('Or simply drag & drop the file here'); ?></span> -->
	<div id="ic-scroll">
		<span class="ic-scroll-arrow first"></span>
		<span class="ic-scroll-arrow second"></span>
		<span class="ic-scroll-arrow third"></span>
	</div>
	<span class="button-subtitle text-secondary protection"><?php pll_e('SSL secured. Your data is safe.'); ?></span>
</div>

<nav class="section-nav">
	<ul>
<?php
foreach ($technology_sections as $section) {
?>
		<li><a href="#<?php echo get_section_href($section->post_title); ?>" class="nav-link technology-link"><span><?php echo $section->post_title; ?></span></a></li>
<?php
}
?>
	</ul>
</nav>

<div id="<?php echo $technology_title; ?>" class="technologies"> 
<?php
foreach ($technology_sections as $section) {
?>
	<section id="<?php echo get_section_href($section->post_title); ?>" class="image-section technology-section" style="background-image: url('<?php echo wp_get_attachment_url(get_post_thumbnail_id($section->post_id), 'medium' ); ?>');">
		<div class="container">
			<h2><?php echo $section->post_title; ?></h2>
			<p><?php echo $section->post_content; ?></p>
		</div>
	</section>
<?php
}
?>
</div>
<?php
foreach ($sections as $section) {
?>
<section id="<?php echo get_section_href($section->post_title); ?>" class="image-section" style="background-image: url('<?php echo wp_get_attachment_url(get_post_thumbnail_id($section->post_id), 'medium' ); ?>');">
	<div class="container">
		<h2><?php echo $section->post_title; ?></h2>
		<p><?php echo $section->post_content; ?></p>
	</div>
</section>
<?php
}

//Fetch the theme footer template 
get_footer(); ?>