<?php
/* 
 *	This page is used to display Technologies page.
*/
 
// Fetch theme header template
get_header();

?>

<nav class="section-nav">
	<ul>
		<li><a href="#fdm"><span>FDM/FFF</span></a></li>
		<li><a href="#sla"><span>SLA</span></a></li>
		<li><a href="#polyjet"><span>PolyJet</span></a></li>
		<li><a href="#sls"><span>SLS</span></a></li>
		<li><a href="#dmls"><span>DMLS</span></a></li>
	</ul>
</nav>

<section id="fdm" class="image-section">
	<div class="container">
		<h2>FDM/FFF</h2>
		<p>The first method is to increase the limit of file upload for PHP in XAMPP / WAMP. This method of increasing the PHP file upload limit is good for middle size files eg. files of size 10Mb to 100Mb or sometimes even files up to 200MB because very large files can hang up the browser.</p>
	</div>
</section>
	
<section id="sla" class="image-section">
  <div class="container">
		<h2>SLA</h2>
		<p>The first method is to increase the limit of file upload for PHP in XAMPP / WAMP. This method of increasing the PHP file upload limit is good for middle size files eg. files of size 10Mb to 100Mb or sometimes even files up to 200MB because very large files can hang up the browser.</p>
	</div>
</section>
	
<section id="polyjet" class="image-section">
  <div class="container">
		<h2>PolyJet</h2>
		<p>The first method is to increase the limit of file upload for PHP in XAMPP / WAMP. This method of increasing the PHP file upload limit is good for middle size files eg. files of size 10Mb to 100Mb or sometimes even files up to 200MB because very large files can hang up the browser.</p>
	</div>
</section>
	
<section id="sls" class="image-section">
  <div class="container">
		<h2>SLS</h2>
		<p>The first method is to increase the limit of file upload for PHP in XAMPP / WAMP. This method of increasing the PHP file upload limit is good for middle size files eg. files of size 10Mb to 100Mb or sometimes even files up to 200MB because very large files can hang up the browser.</p>
	</div>
</section>
	
<section id="dmls" class="image-section">
	<div class="container">
		<h2>DMLS</h2>
		<p>The first method is to increase the limit of file upload for PHP in XAMPP / WAMP. This method of increasing the PHP file upload limit is good for middle size files eg. files of size 10Mb to 100Mb or sometimes even files up to 200MB because very large files can hang up the browser.</p>
	</div>
</section>


<?php
//Fetch the theme footer template 
get_footer(); ?>