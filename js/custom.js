/**
 *	jQuery scripts
 */
jQuery(document).ready(function($){ 

var hash = window.location.hash;
history.scrollRestoration = "manual";

if(hash) {
	var target = $(hash).offset().top;
	console.log(hash, target);
	$("html").animate({scrollTop:target}, 0);
}

/**
 *	Background slideshow init
 */
if ( $("body").hasClass("home") ) {
	var slides = document.querySelectorAll(".slides .slide");
	var currentSlide = 0;

	setInterval(function(){
		if (slides.length!==0) {
		    slides[currentSlide].className = 'slide';
		    currentSlide = (currentSlide+1)%slides.length;
		    slides[currentSlide].className = 'slide showing';
		}
	}, 8000);

	/* Go next after file is uploaded */
	window.setInterval(function(){
		if( !($(".files_uploaded").is(":empty")) ) {
			$(".nf-next").trigger("click");
			$("body").addClass("form-on");
			gtag('event', "File uploaded", {
				'event_category': "Request form",
				'value': 1
			});
		}
	}, 100);
}

/* Make some space for Facebook Messenger widget */
window.setInterval(function(){
	if ( $("#fb-root iframe").width() > 0 ) {
		$("body").addClass("fb-on");
	}
	else if ( $(".language_switcher").hasClass("fb-on") ) {
		$("body").removeClass("fb-on");
	}
}, 100);

/* Language switcher */
$(".language_switcher>button").click(function() {
  $(".language_switcher").toggleClass("open");
});

/* Mobile menu icon */
$(".nav-icon").click(function() {
	$(".nav-icon").toggleClass("open");
	$(".mobile-navigation").toggleClass("open");
	if ( !$("body").hasClass("open") ) {
		setTimeout(function(){
			$(".mobile-navigation").toggleClass("scroll");
			$(".nav-icon").css("right", getScrollbarWidth());
			$("body").toggleClass("open");
		}, 400);
	}
	else {
		$(".mobile-navigation").toggleClass("scroll");
		$(".nav-icon").css("right", 0);
		$("body").toggleClass("open");
	}
});



/**
 *	Full page side nav v1.0
 */
var sections = $("a[href^=\\#][class^='nav-link']");
// console.log("sections",sections);

// var sections = $("a[href*=#]");
// var sections = $("a[href^=\\#]");
// var sections = $("a");

sections.click(function(event) {
		var hash = $(this).attr("href");
		// var target = $(hash).offset().top-200;
		var target = $(hash).offset().top;
		var location = "";
		event.preventDefault();

		if ($("body").hasClass("form-on")) {
			/* Reload when sections are not visible */
			location = window.location.protocol + "//" + window.location.host + window.location.pathname + hash;
			// console.log("loc",location);
			window.location.assign(location);
			window.location.reload();
		}
		else {
			location = window.location.protocol + "//" + window.location.host + window.location.pathname + hash;
			console.log("target",target);
			hashClick(target, hash);
		}
	});

function hashClick(target, hash){
	var time = 500;
	$("html").animate({scrollTop:target}, time);
	$(".nav-icon").removeClass("open");
	$(".mobile-navigation").removeClass("open");
	$("body").removeClass("open");
	
	setTimeout(function(){
		window.location.hash = hash;
	}, time);
	return;
}

function getTargetTop(elem){
	var id = elem.attr("href");
	var offset = $(window).height()/-2;
	return $(id).offset().top - offset;
}

function isSelected(scrolledTo){
	var threshold = $(window).height();
	var i;

	for (i = 0; i < sections.length; i++) {
		var section = $(sections[i]);
		var target = getTargetTop(section);

		if (i+1 == sections.length) {
			if (scrolledTo > target - threshold && scrolledTo < target ) {
				sections.removeClass("active");
				section.addClass("active");
			}
		} else {
			if (scrolledTo > target - threshold && scrolledTo < target + threshold) {
				sections.removeClass("active");
				section.addClass("active");
			}
		}
	};
	return;
}

/* Scroll, sticky topbar */
var topPosition = 0;
 
$(window).on('scroll', function() {
    var scrollMovement = $(window).scrollTop();
   
    if (topPosition < 200 ){
    	$("body").removeClass("scrolled");
    } else {
	    if(scrollMovement > topPosition) {
	          $("#masthead").removeClass("show-header");
	          $("#masthead").addClass("hide-header");
	    } else {
	          $("#masthead").removeClass("hide-header");
	          $("#masthead").addClass("show-header");
	    }
    }

	if ( topPosition > 400 && !$("body").hasClass("scrolled") ) {
		// setTimeout(function(){
			$("body").addClass("scrolled");
		// }, 400);
	}

    topPosition = scrollMovement;

	if ( $("body").hasClass("home") ) {
		isSelected($(window).scrollTop());
	}

	if ($("nav.section-nav ul li a").hasClass("active")) {
		$("nav.section-nav").addClass("active");
	} else {
		$("nav.section-nav").removeClass("active");
	}
	
});

});


/**
 *	Load Facebook SDK for JavaScript
 */
jQuery(window).on('load', function () {
  setTimeout(function(){
    jQuery.getScript('/wp-content/themes/storefront-child/js/fb.js', function() {});
  }, 2000);
});

/**
 *	End of jQuery scripts
 */

/**
 *	Get scrollbar width if available
 */
function getScrollbarWidth() {
  return window.innerWidth - document.documentElement.clientWidth;
}