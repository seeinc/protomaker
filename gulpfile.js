var gulp = require('gulp');
var sass = require('gulp-sass');
var uglifyes = require('uglify-es');
var composer = require('gulp-uglify/composer');
var uglify = composer(uglifyes, console);
var gulpIf = require('gulp-if');
var useref = require('gulp-useref');
var cssnano = require('gulp-cssnano');
var autoprefixer = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');
var util = require('gulp-util');
var plumber = require('gulp-plumber');

gulp.task('sass', function () {
	return gulp.src('scss/custom.scss')
		.pipe(plumber({
			errorHandler: function (err) {
				util.log(util.colors.bgRed('ERROR:'), ' ' + util.colors.red(err.message));
				util.beep();
				this.emit('end');
			}
		}))
		.pipe(sourcemaps.init())
		.pipe(sass())
		.pipe(autoprefixer({
			cascade: true
		}))
		.pipe(sourcemaps.write('.'))
		.pipe(gulp.dest('css/'));
});

gulp.task('watch', function () {
	gulp.watch('scss/**/*.scss', gulp.series(['sass']));
});

gulp.task('build', function () {
	return gulp.src('*.php')
		.pipe(useref())
		.pipe(gulpIf('js/*.js', uglify()))
		.pipe(gulpIf('css/*.css', cssnano({
			discardComments: { removeAll: true }
		})))
		.pipe(gulp.dest('release/'))
});