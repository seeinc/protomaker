<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package storefront-child
 */

?>

		</div><!-- .col-full -->
	</div><!-- #content -->

	<!-- Load Facebook SDK for JavaScript -->
<!--       <script defer>
        jQuery(window).on('load', function () {
          setTimeout(function(){
            jQuery.getScript('/wp-content/themes/storefront-child/js/fb.js', function() {});
          }, 2000);
        });
      </script> -->

      <div id="fb-root"></div>
      <!-- Your Chat Plugin code -->
      <div class="fb-customerchat"
        attribution=setup_tool
        page_id="107951414412157"
  theme_color="#312782">
      </div>

	<?php do_action( 'storefront_before_footer' ); ?>

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="col-full flex-container">

			<?php
			/**
			 * Functions hooked in to storefront_footer action
			 *
			 * @hooked storefront_footer_widgets - 10
			 * @hooked storefront_credit         - 20
			 */
			do_action( 'storefront_footer' );
			?>

		</div><!-- .col-full -->
	</footer><!-- #colophon -->



	<?php do_action( 'storefront_after_footer' ); ?>

</div><!-- #page -->

<link rel="stylesheet" href="https://use.typekit.net/ldo4vrb.css">

<?php wp_footer(); ?>

</body>
</html>
